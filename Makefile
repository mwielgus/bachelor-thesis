.PHONY: all preview

all:
	pandoc \
		--from=markdown+definition_lists+compact_definition_lists \
		--to=latex \
		--output=bachelor_thesis.latex \
		--columns=72 \
		--standalone \
		--listings \
		--top-level-division=chapter \
		--metadata=classoption:en,11pt \
		--metadata=documentclass:aghdpl \
		--metadata=title-meta:'Asynchronous incrementing memory with a synchronous controller' \
		--metadata=author-meta:'Mikołaj Wielgus' \
		--metadata=date-meta:2020 \
		--include-in-header=aghdpl_setup.latex \
		--include-in-header=listings_setup.latex \
		--include-before-body=aghdpl_body.latex \
		--filter=pandoc-crossref \
		--metadata=figPrefix:Fig. \
		--metadata=eqnPrefix:Eq. \
		--metadata=tblPrefix:Table \
		--metadata=lstPrefix:Listing \
		--metadata=secPrefix:Section \
		--metadata=codeBlockCaptions:true \
		--metadata=listings:true \
		--citeproc \
		--bibliography=bibliography.yaml \
		--csl=ieee.csl \
		bachelor_thesis.md
	pdflatex bachelor_thesis.latex
		#--number-sections \

preview: all
	evince bachelor_thesis.pdf
