% Asynchronous incrementing memory with a synchronous controller
% Mikołaj Wielgus
% 2020

# Introduction

## Background

Hybrid X-ray photon counting (HPC) detectors are a relatively new approach to X-ray measurement
@radiation-imaging-2020. During the last two decades they have greatly improved X-ray detection for
many applications over a wide energy spectrum range. Their remarkable properties, such as high
dynamic range, the lack of readout noise and the possibility of processing signals from single
photons, have proven useful in numerous fields of research, such as plasma spectroscopy, X-ray
spectroscopy and chemical crystallography.

## General description

A typical HPC system consists of an array of pixels. Each pixel is composed of an inversely
polarized semiconductor diode acting as a sensor and an analog readout front-end. An X-ray photon
incident on the sensor (@fig:hpc-pixel) generates a charge cloud which is carried to the readout
channel, producing an impulse with amplitude proportional to the photon energy
@hpc-detectors-2019.

![Side view of an HPC pixel @hpc-detectors-2019](fig/hpc_pixel.jpg){#fig:hpc-pixel width=70%
test=test}

To allow further processing, the impulse has to be digitized and processed. This is usually
performed by passing the analog readout to several discriminators, each connected to a
counter @hpc-asics-2016. Less often, the readout is digitized with a complete ADC, and its
conversion result is processed with a more sophisticated digital back-end.

## Target system

The purpose of this work is to develop a module suitable for use as the digital back-end of an HPC
system that employs ADCs. A single channel of the target system is shown in @fig:hpc-processing. To
reduce interference, no clock is used for synchronization. Instead, the back-end is asynchronously
triggered by the readout signal passed through a clipper circuit, producing a valid digital signal
from a variable-amplitude impulse.

![Block diagram of the target system](fig/hpc_processing.eps){#fig:hpc-processing width=90%}

The back-end has its own memory, which is addressed using the ADC conversion result. When
triggered, the addressed cell is incremented. Hence, after a measurement is done, the memory will
store a histogram of the photon energies, which is the energy spectrum of the incident radiation,
basically a quantized version of the electromagnetic spectrum.

Once the measurement is done, the data has to be extracted from each pixel. The memories also have
to be cleared before every measurement. These operations, performed by the back-end, can be
implemented synchronously, as a clock will not interfere when not in the measurement phase.

# Specification

The designed module is capable of:

- Asynchronous incrementation of an SRAM cell,
- Synchronous reading of cells with automatic address incrementation,
- Optionally clearing the memory cell after reading.

Each of these functionalities has a dedicated mode of operation described in
@sec:modes-of-operation. The communication interface is specified in @sec:interface.

The design does not implement an SRAM. Instead, it has to be able to use any SRAM module that
conforms to the specification in @sec:sram-module.

## SRAM module {#sec:sram-module}

The SRAM module symbol is depicted in @fig:sram-symbol and its ports are described in
@tbl:sram-ports. Communication with this module is performed using the four-phase handshake
protocol through the `read`, `read_done`, `write`, `write_done` ports.

The timing sequences are shown in @fig:sram-read-timing and @fig:sram-write-timing. The timing
characteristics of the SRAM module have not been determined precisely, depend on the technology and
are largely irrelevant to this work. For convenience, it is assumed that

- $t_\mathrm{setup} \geq 2 \; \mathrm{ns}$,
- $t_\mathrm{hold} \leq 0 \; \mathrm{ns}$,
- $t_\mathrm{w}, t_\mathrm{wb}, t_\mathrm{r}, t_\mathrm{rb} \approx 1 \; \mathrm{ns}$.

![SRAM module symbol](fig/sram_symbol.png){#fig:sram-symbol}

\clearpage

: SRAM module ports {#tbl:sram-ports}

+--------------+----------+-------------------------------------------------------+
| **Port**     | **Type** | **Description**                                       |
+==============+==========+=======================================================+
| `VDD`        | power    | Positive supply rail.                                 |
+--------------+----------+-------------------------------------------------------+
| `VSS`        | power    | Negative supply rail.                                 |
+--------------+----------+-------------------------------------------------------+
| `addr[5:0]`  | input    | Address of the selected cell.                         |
+--------------+----------+-------------------------------------------------------+
| `din[11:0]`  | input    | Value to be written during writing.                   |
+--------------+----------+-------------------------------------------------------+
| `dout[11:0]` | output   | Value read during reading.                            |
+--------------+----------+-------------------------------------------------------+
| `read`       | input    | Rising edge initiates reading.                        |
+--------------+----------+-------------------------------------------------------+
| `read_done`  | output   | Rising edge indicates that reading has been finished. |
+--------------+----------+-------------------------------------------------------+
| `write`      | input    | Rising edge initiates writing.                        |
+--------------+----------+-------------------------------------------------------+
| `write_done` | output   | Rising edge indicates that writing has been finished. |
+--------------+----------+-------------------------------------------------------+

![SRAM read timing sequence](fig/sram_read_timing.eps){#fig:sram-read-timing}

![SRAM write timing sequence](fig/sram_write_timing.eps){#fig:sram-write-timing}

## Interface {#sec:interface}

@fig:module-symbol depicts the symbol of the designed digital back-end. Its ports and their
functions are described in @tbl:module-ports.

![Digital back-end symbol](fig/module_symbol.png){#fig:module-symbol width=70%}

\clearpage

: Digital back-end module ports {#tbl:module-ports}

+------------+----------+----------+-------------------------------------------------------------+
| **Port**   | **Dir.** | **Type** | **Description**                                             |
+============+==========+==========+=============================================================+
| clk        | in       | clock    | Active on rising edge.                                      |
+------------+----------+----------+-------------------------------------------------------------+
| incr       | in       | async.   | Requests incrementation of the cell at `addr`. Active on    |
|            |          |          | rising edge.                                                |
+------------+----------+----------+-------------------------------------------------------------+
| addr[5:0]  | in       | sync.    | The address of the selected cell. May be different than the |
|            |          |          | actual address of the current cell in READ and READZ modes. |
+------------+----------+----------+-------------------------------------------------------------+
| mode[2:0]  | in       | sync.    | Possible values:                                            |
|            |          |          |                                                             |
|            |          |          | - **'b000** (IDLE): Do nothing.                             |
|            |          |          | - **'b001** (INCR): Increment the selected cell.            |
|            |          |          | - **'b010** (READ): Read the selected cell.                 |
|            |          |          | - **'b100** (READZ): Like READ,                             |
|            |          |          | but the cell is also cleared.                               |
+------------+----------+----------+-------------------------------------------------------------+
| read       | in       | sync.    | Triggers reading of the memory in the READ and READZ modes. |
+------------+----------+----------+-------------------------------------------------------------+
| dout[11:0] | out      | sync.    | The data read during READ and READZ operations.             |
+------------+----------+----------+-------------------------------------------------------------+

## Modes of operation {#sec:modes-of-operation}

### IDLE mode

No operations are performed during the IDLE mode. Changing modes without going through this mode is
forbidden; for example, it is not allowed to switch from INCR to READ without going through IDLE.
Selected by setting `mode`='b000.

### INCR mode

Asynchronously increments the cell under `addr`. Triggered by a rising edge of `incr`. Selected by
setting `mode=`'b001.

### READ mode

Synchronously reads the current cell and outputs it on `dout`. The output data from the SRAM is
latched. The current cell address is automatically incremented after reading. Selected by setting
`mode`='b010.

### READZ mode

Synchronously reads the current cell and outputs it on `dout`. Once reading is done, the cell is
cleared, and then the current cell address is automatically incremented. Selected by setting
`mode`='b100.

# Implementation

## Top-level schematic

@fig:module-schematic is the schematic of the entire digital back-end module. On the left, there
are two controllers in charge of generating the proper timing sequences for the SRAM module. Only
one module can be active at a time, as selected by `mode`.

![Top-level schematic](fig/module_schematic.png){#fig:module-schematic height=95%}

`I3` is the asynchronous controller responsible for the INCR mode, active when `incr_active`, which
is `incr` latched by the `I13` D flip-flop, is high. Once activated, the asynchronous controller
performs a four-phase handshake with the SRAM module `I46`. When the sequence is complete, the
D flip-flop is reset by the `write_done` signal. @sec:async-ctrl describes the asynchronous
controller operation in detail.

`I42` is the synchronous controller active during the READ and READZ modes. It is a simple state
machine with two independent sets of states, one for each of the applicable modes, with each
transition activated by a positive `clk` edge. @sec:sync-ctrl describes the synchronous controller
operation in detail.

Since both controllers have to control the same SRAM input, their outputs are combined using NAND
gates `I23[11:0]`, `I24`, `I25` effectively acting as simplified multiplexers.

The automatic address incrementation is performed by the circuit shown in the upper region of the
schematic. Before a read signal arrives in the SRAM module, the `I16[5:0]` D flip-flops latch the
address from `addr_muxed[5:0]`, which is initially the same as `addr[5:0]`, to `addr_active[5:0]`.
At the same time, the `cont` net will be latched high, causing the `I32[5:0]` multiplexers to
change `addr_muxed[5:0]` to an incremented value of the currently active address
(`addr_active[5:0]`). Subsequent read cycles will latch this value instead, resulting in the
currently active address being incremented every time a READ or READZ operation is executed. This
will cease when `mode[2:0]` changes to IDLE.

## Asynchronous controller {#sec:async-ctrl}

The asynchronous controller (@fig:async-ctrl-impl) performs memory cell incrementation when the
INCR mode is active. Signal sequencing is provided by the four-state asynchronous state machine
composed of the gates shown in the circuit diagram, with the `s_1` and `s_0` nets representing the
state bits, which can hold the four possible controller states: IDLE (`s_1,s_0`='b11), READ
(`s_1,s_0`='b10), WAIT (`s_1,s_0`='b00), WRITE (`s_1,s_0`='b01). The asynchronous controller state
diagram is shown in @fig:async-ctrl-state-diagram and the timing diagrams of its ports during one
cycle of operation are shown in @fig:incr-mode-waveforms.

![Asynchronous controller schematic](fig/async_ctrl_impl.png){#fig:async-ctrl-impl}

The incrementation of the cell value is directly performed by the increment adder `I21`, whose
internal schematic is shown in @fig:incr12-impl. Bearing in mind the target application, the
circuit was designed to minimize the area. The half-adders used are shown in @fig:halfadder2-impl.

![Increment adder schematic](fig/incr12_impl.png){#fig:incr12-impl width=70%}

![Single half-adder schematic](fig/halfadder2_impl.png){#fig:halfadder2-impl}

To place the circuit in a well-defined initial state, `read_in` is combined with `incr` through the
`I31` NAND gate. When `incr`=0, both inputs of the `I19` NAND gate will be high, causing its output
to become low and in turn setting `s_0` and `s_1` to 1, which is the IDLE state. This reset
functionality will activate regardless of the state the controller is in.

![Asynchronous controller state
diagram](fig/async_ctrl_state_diagram.eps){#fig:async-ctrl-state-diagram width=70%}

![Timing diagram for the INCR mode](fig/incr_mode_waveforms.eps){#fig:incr-mode-waveforms
width=70%}

The gate-level schematic of the asynchronous controller has been synthesized using the techniques
described in @digital-design-a-systems-approach. @fig:async-ctrl-full-k-map shows the Karnaugh map
of the entire state machine, with the possible state transitions marked using arrows.

@fig:async-ctrl-s-0-k-map and @fig:async-ctrl-s-1-k-map show the Karnaugh maps that were used to
synthesize the next-state logic for the `s_0` and `s_1` state bits, respectively. The selected
groups of ones all intersect each other, preventing races. The resulting Boolean expressions are

$$ s_0^\mathrm{nxt} = \bar{s_1}\bar{s_0} \lor \bar{i}\bar{r} \lor \bar{r}\bar{s_1} $$
$$ s_1^\mathrm{nxt} = s_1s_0 \lor \bar{i}\bar{r} \lor \bar{r}\bar{s_1}, $$

where $s_1$, $s_0$ are respectively the `s_1` and `s_0` state bits, $i$, $r$ are respectively the
`incr` and `read` inputs, and $^\mathrm{nxt}$ denotes the next value.

The boolean expressions describing the outputs as functions of the state bits, derived using
ordinary logic synthesis techniques, are shown below:

$$ \mathrm{read\_b} = s_1\bar{s_0} $$
$$ \mathrm{write\_b} = \bar{s_1}s_0. $$

![Karnaugh map of the entire state
machine](fig/async_ctrl_full_k_map.eps){#fig:async-ctrl-full-k-map}

![Karnaugh map of the `s_0` state bit](fig/async_ctrl_s_0_k_map.eps){#fig:async-ctrl-s-0-k-map}

![Karnaugh map of the `s_1` state bit](fig/async_ctrl_s_1_k_map.eps){#fig:async-ctrl-s-1-k-map}

## Synchronous controller {#sec:sync-ctrl}

The synchronous controller is a state machine with four possible states: IDLE, READ, ZERO, WRITE.
On every positive `clk` edge a state transition occurs. The new state is determined by the `mode`
and `en` controller inputs, respectively connected to the `mode[2:1]` and `read` top-level
digital back-end module ports. The state diagram is shown in @fig:sync-ctrl-state-diagram.

There are three possible cycles the state machine can loop over:

- IDLE $\rightarrow$ IDLE, used in the IDLE and INCR modes,
- IDLE $\rightarrow$ READ $\rightarrow$ IDLE, used in the READ mode,
- IDLE $\rightarrow$ READ $\rightarrow$ ZERO $\rightarrow$ WRITE, used in the READZ mode. 

The timing diagrams of the READ and READZ modes are shown in @fig:read-mode-waveforms and
@fig:readz-mode-waveforms respectively.

![Synchronous controller state
diagram](fig/sync_ctrl_state_diagram.eps){#fig:sync-ctrl-state-diagram width=70%}

![Timing diagram for the READ mode](fig/read_mode_waveforms.eps){#fig:read-mode-waveforms
width=70%}

![Timing diagram for the READZ mode](fig/readz_mode_waveforms.eps){#fig:readz-mode-waveforms
width=70%}

# Verification

## Background

A set of tests has been created to ensure the proper functioning of the digital back-end. They are
described in @tbl:tests. Each test runs a digital simulation of the circuit with a specific input
sequence and compares the outputs to a predefined pattern. If there is no difference, then the test
is passed. Otherwise, a failure is reported.

All tests are behavioral (black-box), meaning that they do not examine any signals inside the
designed module, only checking the top-level outputs. The SystemVerilog @systemverilog testbench
used for the verification can be found in the appendix in @lst:testbench.

Simulation waveforms of the internal signals, with some additional commentary, are shown in
@sec:operation.

\clearpage

: List of tests {#tbl:tests}

+-------------------------------------------+-----------------------------------------------------+
| **Test name**                             | **Testing procedure**                               |
+===========================================+=====================================================+
| `TEST_INCR_MODE`                          | Test INCR mode.                                     |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_READ_MODE`                          | Test READ mode. Also test stopping read before all  |
|                                           | necessary clock cycles happen.                      |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_READZ_MODE`                         | Test READZ mode. Also test stopping read before all |
|                                           | necessary clock cycles happen.                      |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_ADDR_INCR`                          | Test address incrementation.                        |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_READ_ALL`                           | Initialize then read the entire memory.             |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_OVERFLOW_CELL`                      | Test integer overflow of the value of a memory      |
|                                           | cell.                                               |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_OVERFLOW_ADDR`                      | Test integer overflow of the active address.        |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_RANDOM_WRITE_READ`                  | Write cells randomly, then test their values.       |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_TOTAL`                              | Test complete digital back-end operation cycle,     |
|                                           | twice.                                              |
+-------------------------------------------+-----------------------------------------------------+
| `TEST_TOTAL_BRIEF`                        | Simplified version of `TEST_TOTAL`, used in         |
|                                           | @sec:complete-operation.                            |
+-------------------------------------------+-----------------------------------------------------+

## Operation {#sec:operation}

### INCR mode

@fig:sim-incr-mode-waveforms shows simulation waveforms of the top-level schematic nets during two
cycles of the INCR mode. An impulse on `incr` initiates the sequence, setting `incr_active` high.
After some delay, `addr` gets latched to `addr_active`, and an impulse on `read_active` follows
almost immediately after, going through the SRAM module and coming out on `read_done`. The data on
`dout_pre[11:0]` (SRAM module data output) is latched to `dout[11:0]` on the positive edge of
`read_done`, as it indicates that the data has become valid.

After a short pause (WAIT state), an analogical situation occurs on `write_active` and
`write_done`, except that the data is obviously not latched. The impulses are also longer due to an
additional delay present on the `incr_active` reset path. Once `incr_active` is cleared, the state
machine returns to its initial IDLE mode. A cycle is complete when `write_done` turns low.

The simulation waveforms presented in this section are consistent with the timing diagram in
@fig:incr-mode-waveforms.

![Simulated INCR mode waveforms](fig/sim_incr_mode_waveforms.png){#fig:sim-incr-mode-waveforms}

### READ mode {#sec:read-mode-operation}

@fig:sim-read-mode-waveforms shows simulation waveforms of the top-level schematic nets during four
consecutive cycles of the READ mode. A positive `clk` edge puts the synchronous controller in the
READ state, changing `read_active` to high. This change, once it propagates through the SRAM module
and reappears on `read_done`, causes `dout_pre[11:0]` to get latched to `dout[11:0]`. The data on
the output remains valid for the entire remaining duration of the READ state and the subsequent
IDLE state, which is entered on the next positive edge of `clk`. A cycle is completed when
`read_done` turns low.

The simulation waveforms presented in this section are consistent with the timing diagram in
@fig:read-mode-waveforms.

![Simulated READ mode waveforms](fig/sim_read_mode_waveforms.png){#fig:sim-read-mode-waveforms}

### READZ mode

@fig:sim-readz-mode-waveforms shows simulation waveforms of the top-level schematic nets during two
cycles of the READZ mode. For the first two states, IDLE and READ, the behavior is identical to
the behavior in the READ mode (see @sec:read-mode-operation). However, after the READ state, two
more states, ZERO and WRITE, are entered on positive `clk` edges before the IDLE state is reached.

The ZERO state has outputs that are the same as in the IDLE state, meaning that `read_active` and
`write_active` are low. The only functional difference is that it is the WRITE state that follows.
During the WRITE state, `data_active[11:0]` is cleared, and then `write_active` is raised,
triggering data writing. `write_active` becomes low once again when the state machine returns to
the IDLE state, ending a cycle.

The simulation waveforms presented in this section are consistent with the timing diagram in
@fig:readz-mode-waveforms.

![Simulated READZ mode waveforms](fig/sim_readz_mode_waveforms.png){#fig:sim-readz-mode-waveforms}

### Complete operation sequence {#sec:complete-operation}

The top-level net waveforms generated by a simplified simulation of two complete operation cycles
are shown in @fig:sim-total-waveforms. First, the memory is cleared using READZ mode. Then, random
memory cells are incremented several times using INCR mode, and finally, the memory is read using
the READ mode.

To make the waveforms easier to read, only four memory cells are used in the shown simulation, and
they are randomly incremented only six times. This simulation is performed in the test
`TEST_TOTAL_BRIEF`. A complete version of this test, where all the memory cells are used,
incrementation is performed 10000 times and the entire procedure is performed twice, is
`TEST_TOTAL`.

![Simplified complete operation sequence
waveforms](fig/sim_total_waveforms.png){#fig:sim-total-waveforms height=95%}

## Result

All tests passed successfully.

# Conclusion

A synthesizable module, suitable for use as the digital back-end of an asynchronous HPC system
employing ADCs, has been presented. It should be possible to synthesize this design, with little to
no adjustments, in most of the available silicon technologies supporting digital circuits. The
presented simulations prove that the design fulfills the specification.

As with most designs, there are many ways this circuit can be improved. For example, the delay
lines could be replaced with more sophisticated synchronization circuits. To ensure that the
circuit operates properly regardless of the delays of its digital elements, the hard-coded internal
delay values could be fuzzed (repeatedly tested with random values).

This thesis has been written in Pandoc Markdown and converted to PDF using Pandoc and PDFLaTex. Dia
and Inkscape were used to create the figures and ImageMagick was used to crop PNGs generated by the
schematic editor. The schematic was developed and simulated using the Cadence Virtuoso proprietary
software suite.

# References

::: {#refs}
:::

\appendix

# Appendix: SystemVerilog code {#sec:appendix}

: SystemVerilog implementation of the synchronous controller. {#lst:sync-ctrl-impl}

```verilog
module sync_ctrl(
	output reg read_b, write_b, zero_data_b,
	input wire clk,
	input wire [1:0] mode,
	input wire en
);
	localparam IDLE_MODE = 'b00;
	localparam READ_MODE = 'b01;
	localparam READZ_MODE = 'b10;

	localparam IDLE_STATE = 0;
	localparam READ_STATE = 1;
	localparam ZERO_STATE = 2;
	localparam WRITE_STATE = 3;
	
	reg read_b_nxt, write_b_nxt, zero_data_b_nxt;
	reg [2:0] state, state_nxt;

	always @* begin
		#1;
		zero_data_b_nxt = mode == READZ_MODE ? 0 : 1;

		case (state_nxt)
			IDLE_STATE: {read_b_nxt, write_b_nxt} = 'b11;
			READ_STATE: {read_b_nxt, write_b_nxt} = 'b01;
			ZERO_STATE: {read_b_nxt, write_b_nxt} = 'b11;
			WRITE_STATE: {read_b_nxt, write_b_nxt} = 'b10;
		endcase
	end

	always @* begin
		case (state)	
			default: #1 state_nxt = (en && (mode == READ_MODE || mode == READZ_MODE)) ? READ_STATE : IDLE_STATE;
			READ_STATE: #1 state_nxt = (mode == READZ_MODE) ? ZERO_STATE : IDLE_STATE;
			ZERO_STATE: #1 state_nxt = WRITE_STATE;
			WRITE_STATE: #1 state_nxt = IDLE_STATE; 
		endcase
	end

	always @(posedge clk) begin
		#1;
		state <= state_nxt;
		read_b <= read_b_nxt;
		write_b <= write_b_nxt;
		zero_data_b <= zero_data_b_nxt;
	end
endmodule
```

\clearpage

: SystemVerilog testbench {#lst:testbench}

```verilog
module async_incr_mem_64x12_tb#(
	localparam ADDR_SIZE = 6,
	localparam MODE_SIZE = 3,
	localparam WORD_SIZE = 12
)(
	output reg [ADDR_SIZE-1:0] addr,
	output reg clk,
	output reg incr,
	output reg [MODE_SIZE-1:0] mode,
	output reg read,
	output wire vdd, vss,
	input wire [WORD_SIZE-1:0] dout
);
	localparam IDLE_MODE = 'b000;
	localparam INCR_MODE = 'b001;
	localparam READ_MODE = 'b010;
	localparam READZ_MODE = 'b100;
	
	localparam TEST_COUNT = 9;

	int i, ii;
	int test_order[TEST_COUNT];
	int cell_values[2**ADDR_SIZE-1:0];
	reg [5:0] initial_addr;

	assign vdd = 1;
	assign vss = 0;	

	initial begin
		clk = 0;
		forever #10 clk = ~clk;
	end

	initial begin
		addr = 0;
		incr = 0;
		mode = IDLE_MODE;
		read = 0;

		foreach (test_order[i]) begin
			test_order[i] = i;
		end

		test_order.shuffle();
		
		foreach (test_order[i]) begin
			case (test_order[i])
				0: TEST_INCR_MODE();
				1: TEST_READ_MODE();
				2: TEST_READZ_MODE();
				3: TEST_ADDR_INCR();
				4: TEST_READ_ALL();
				5: TEST_OVERFLOW_CELL();
				6: TEST_OVERFLOW_ADDR();
				7: TEST_TOTAL_BRIEF();
				8: TEST_TOTAL();
			endcase
		end

		$stop;
	end

	task change_mode;
	input [MODE_SIZE-1:0] _mode;
	begin
		#1 mode = IDLE_MODE;

		@(negedge clk);
		#1 mode = _mode;
	end
	endtask

	task do_incr;
	begin
		@(negedge clk);
		#1 incr = 1;
		
		@(negedge clk);
		#1 incr = 0;
		
		repeat(2) @(negedge clk);
	end
	endtask

	task do_read(bit cont = 0, int cycles = 2);
	begin
		#1 read = 1;
		repeat(cycles) @(negedge clk);
		
		if (!cont) begin
			#1 read = 0;
		end
	end
	endtask
	
	task do_readz(bit cont = 0, int cycles = 4);
	begin
		#1 read = 1;
		repeat(cycles) @(negedge clk);
		
		if (!cont) begin
			#1 read = 0;
		end
	end
	endtask

	task finish_read;
	begin
		#1 read = 0;
	end
	endtask

	task TEST_INCR_MODE;
	begin
		$display("\nTesting INCR mode...");
		
		#1 addr = $urandom_range(2**ADDR_SIZE-1, 0);
		change_mode(READZ_MODE);
		do_readz();

		// Test whether launching INCR mode itself does not change the cell.
		change_mode(INCR_MODE);
		@(negedge clk);
		change_mode(IDLE_MODE);
		
		change_mode(INCR_MODE);
		do_incr();

		change_mode(READ_MODE);		
		do_read();
		assert(dout == 1);

		$display("Done.");
	end
	endtask

	task TEST_READ_MODE;
	begin
		$display("\nTesting READ mode...");
		
		#1 addr = $urandom_range(2**ADDR_SIZE-1, 0);
		change_mode(READZ_MODE);
		do_readz();

		change_mode(INCR_MODE);
		repeat(2) do_incr();

		change_mode(READ_MODE);
		change_mode(IDLE_MODE);

		change_mode(READ_MODE);
		do_read(0, 1);
		change_mode(IDLE_MODE);

		change_mode(READ_MODE);
		do_read();
		assert(dout == 2);

		$display("Done.");
	end
	endtask

	task TEST_READZ_MODE;
	begin
		$display("\nTesting READZ mode...");

		#1 addr = $urandom_range(2**ADDR_SIZE-1, 0);
		change_mode(READZ_MODE);
		repeat(2) do_readz();

		change_mode(INCR_MODE);
		do_incr();

		change_mode(READZ_MODE);
		do_readz();
		assert(dout == 1);
		
		do_readz();
		assert(dout == 0);

		$display("Done.");
	end
	endtask

	task TEST_ADDR_INCR;
	begin
		$display("\nTesting address incrementation...");
		initial_addr = $urandom_range(2**ADDR_SIZE-1, 0);
		
		#1 addr = initial_addr;
		change_mode(READZ_MODE);
		
		for (i = 0; i < 4; ++i) begin
			do_readz(1);
		end
		finish_read();

		#1 addr = initial_addr;
		change_mode(INCR_MODE);

		for (i = 0; i < 4; ++i) begin
			for (ii = 0; ii < i+1; ii = ++ii) begin
				do_incr();
			end

			#1 ++addr;
		end

		#1 addr = initial_addr;
		change_mode(READ_MODE);

		for (i = 0; i < 4; ++i) begin
			do_read(1);
			assert(dout == i+1);
		end
		finish_read();

		$display("Done.");
	end
	endtask

	task TEST_READ_ALL;
	begin
		$display("\nTesting reading all cells...");

		@(negedge clk);
		#1 addr = 0;
		change_mode(READZ_MODE);

		for (i = 0; i < 2**ADDR_SIZE; ++i) begin
			do_readz(1);
		end
		finish_read();

		change_mode(INCR_MODE);
		
		#1 addr = 0;
		do_incr();

		#1 addr = 2**6-1;
		do_incr();
		
		#1 addr = 0;
		change_mode(READ_MODE);
		
		do_read(1);
		assert(dout == 1);

		for (i = 1; i < 2**ADDR_SIZE-1; ++i) begin
			do_read(1);
			assert(dout == 0);
		end
	
		do_read(1);
		assert(dout == 1);
		finish_read();

		$display("Done.");
	end
	endtask

	task TEST_OVERFLOW_CELL;
	begin
		$display("\nTesting cell overflow...");

		#1 addr = $urandom_range(2**ADDR_SIZE-1, 0);
		change_mode(READZ_MODE);
		do_readz();

		change_mode(INCR_MODE);
		for (i = 0; i < 2**WORD_SIZE-1 ; ++i) begin
			do_incr();
		end

		change_mode(READ_MODE);
		do_read();
		assert(dout == 2**WORD_SIZE-1);
		
		change_mode(INCR_MODE);
		do_incr();

		change_mode(READ_MODE);
		do_read();
		assert(dout == 0);

		$display("Done.");
	end
	endtask
	
	task TEST_OVERFLOW_ADDR;
	begin
		$display("\nTesting address overflow...");

		#1 addr = 2**ADDR_SIZE-1;
		change_mode(READZ_MODE);
		repeat(2) do_readz(1);
		finish_read();

		change_mode(INCR_MODE);
		do_incr();
		
		#1 addr = 0;
		repeat(2) do_incr();

		#1 addr = 2**ADDR_SIZE-1;
		change_mode(READ_MODE);
		
		do_read(1);
		assert(dout == 1);

		do_read(1);
		assert(dout == 2);
		finish_read();
		
		$display("Done.");
	end
	endtask

	task TEST_TOTAL;
	begin
		$display("\nTesting complete circuit operation...");
		
		// Repeat twice.
		for (i = 0; i < 2; ++i) begin
			@(negedge clk);
			#1 addr = 0;
			change_mode(READZ_MODE);

			for (i = 0; i < 2**ADDR_SIZE; ++i) begin
				do_readz(1);
				cell_values[i] = 0;
			end
			finish_read();

			change_mode(INCR_MODE);

			for (i = 0; i < 10000; ++i) begin
				#1 addr = $urandom_range(2**ADDR_SIZE-1, 0);
				do_incr();
				++cell_values[addr];
				if (cell_values[addr] >= 2**WORD_SIZE) begin
					cell_values[addr] = 0;
				end
			end
		
			#1 addr = 0;
			change_mode(READ_MODE);

			for (i = 0; i < 2**ADDR_SIZE; ++i) begin
				do_read(1);
				assert(dout == cell_values[i]);
			end
			finish_read();
		end

		$display("Done.");
	end
	endtask

	task TEST_TOTAL_BRIEF;
	begin
		$display("\nTesting complete circuit operation (brief)...");
		initial_addr = $urandom_range(2**ADDR_SIZE-1-3, 0);

		@(negedge clk);
		#1 addr = initial_addr;
		change_mode(READZ_MODE);
		
		for (i = initial_addr; i < initial_addr+4; ++i) begin
			do_readz(1);
			cell_values[i] = 0;
		end
		finish_read();

		change_mode(INCR_MODE);

		for (i = 0; i < 6; ++i) begin
			#1 addr = $urandom_range(initial_addr+3, initial_addr);
			do_incr();
			++cell_values[addr];
			if (cell_values[addr] >= 2**WORD_SIZE) begin
				cell_values[addr] = 0;
			end
		end
		
		#1 addr = initial_addr;
		change_mode(READ_MODE);

		for (i = initial_addr; i < initial_addr+4; ++i) begin
			do_read(1);
			assert(dout == cell_values[i]);
		end
		finish_read();

		$display("Done.");
	end
	endtask
endmodule
```
