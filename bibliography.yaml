---
references:
- id: hpc-detectors-2019
  source: PubMed
  accessed:
    date-parts:
      - - 2020
        - 12
        - 8
  title: Transforming X-ray detection with hybrid photon counting detectors
  author:
    - family: Förster
      given: Andreas
    - family: Brandstetter
      given: Stefan
    - family: Schulze-Briese
      given: Clemens
  container-title-short: Philos Trans A Math Phys Eng Sci
  container-title: Philosophical transactions. Series A, Mathematical, physical, and engineering sciences
  publisher: The Royal Society Publishing
  ISSN: 1364-503X
  issued:
    date-parts:
      - - 2019
        - 6
        - 17
  page: "20180241"
  volume: "377"
  issue: "2147"
  PMID: "31030653"
  PMCID: PMC6501887
  DOI: 10.1098/rsta.2018.0241
  type: article-journal



- id: radiation-imaging-2020
  type: article-journal
  author:
  - family: Heijne
    given: Erik H.M.
  issued:
  - year: 2020
  title: History and future of radiation imaging with single quantum processing pixel
    detectors
  container-title: Radiation Measurements
  page: '106436'
  abstract: This introductory article treats aspects of the evolution of early semiconductor
    detectors towards modern radiation imaging instruments, now with millions of signal
    processing cells, exploiting the potential of silicon nanotechnology. The Medipix
    and Timepix assemblies are among the prime movers in this evolution. Imaging the
    impacts in the detecting matrix from the individual ionizing particles and photons
    can be used to study these elementary quanta themselves, or allows one to visualize
    various characteristics of objects under irradiation. X-ray imaging is probably
    the most-used modality of the latter, and the new imagers can process each single
    incident X–photon to obtain an image with additional information about the structure
    and composition of the object. The atomic distribution can be imaged, taking advantage
    of the energy-specific X-ray absorption. A myriad of other applications is appearing,
    as reported in the special issue of this journal. As an example, in molecular
    spectroscopy, the sub-nanosecond timing in each pixel can deliver in real-time
    the mapping of the molecular composition of a specimen by time-of-flight for single
    molecules, a revolution compared with classical gel electrophoresis. References
    and some personal impressions are provided to illuminate radiation detection and
    imaging over more than 50 years. Extrapolations and wild guesses for future developments
    conclude the article.
  keyword: Nuclear detector, Energy quantum, Ionizing particle, X-ray, Pixel detector,
    Semiconductor imager, CMOS nanotechnology
  URL: http://www.sciencedirect.com/science/article/pii/S1350448720302146
  DOI: https://doi.org/10.1016/j.radmeas.2020.106436
  ISSN: 1350-4487



- id: hpc-asics-2016
  type: article-journal
  author:
  - family: Ballabriga
    given: R.
  - family: Alozy
    given: J.
  - family: Campbell
    given: M.
  - family: Frojdh
    given: E.
  - family: Heijne
    given: E.H.M.
  - family: Koenig
    given: T.
  - family: Llopart
    given: X.
  - family: Marchal
    given: J.
  - family: Pennicard
    given: D.
  - family: Poikela
    given: T.
  - family: Tlustos
    given: L.
  - family: Valerio
    given: P.
  - family: Wong
    given: W.
  - family: Zuber
    given: M.
  issued:
  - year: 2016
    month: 1
  title: Review of hybrid pixel detector readout ASICs for spectroscopic x-ray imaging
  container-title: Journal of Instrumentation
  publisher: IOP Publishing
  page: P01007-P01007
  volume: '11'
  issue: '01'
  abstract: Semiconductor detector readout chips with pulse processing electronics
    have made possible spectroscopic X-ray imaging, bringing an improvement in the
    overall image quality and, in the case of medical imaging, a reduction in the
    X-ray dose delivered to the patient. In this contribution we review the state
    of the art in semiconductor-detector readout ASICs for spectroscopic X-ray imaging
    with emphasis on hybrid pixel detector technology. We discuss how some of the
    key challenges of the technology (such as dealing with high fluxes, maintaining
    spectral fidelity, power consumption density) are addressed by the various ASICs.
    In order to understand the fundamental limits of the technology, the physics of
    the interaction of radiation with the semiconductor detector and the process of
    signal induction in the input electrodes of the readout circuit are described.
    Simulations of the process of signal induction are presented that reveal the importance
    of making use of the small pixel effect to minimize the impact of the slow motion
    of holes and hole trapping in the induced signal in high-Z sensor materials. This
    can contribute to preserve fidelity in the measured spectrum with relatively short
    values of the shaper peaking time. Simulations also show, on the other hand, the
    distortion in the energy spectrum due to charge sharing and fluorescence photons
    when the pixel pitch is decreased. However, using recent measurements from the
    Medipix3 ASIC, we demonstrate that the spectroscopic information contained in
    the incoming photon beam can be recovered by the implementation in hardware of
    an algorithm whereby the signal from a single photon is reconstructed and allocated
    to the pixel with the largest deposition.
  URL: https://doi.org/10.1088/1748-0221/11/01/p01007
  DOI: 10.1088/1748-0221/11/01/p01007



- id: digital-design-a-systems-approach
  type: book
  author:
  - family: Dally
    given: William J.
  - family: Harting
    given: R. Curtis
  issued:
  - year: 2015
  title: 'Digital Design: A systems approach'
  title-short: Digital Design
  publisher: Cambridge University Press
  DOI: 10.1017/CBO9781316162651



- id: systemverilog
  type: article-journal
  issued:
  - year: 2018
  title: IEEE Standard for SystemVerilog–Unified Hardware Design, Specification, and
    Verification Language
  container-title: IEEE Std 1800-2017 (Revision of IEEE Std 1800-2012)
  page: 1-1315
  DOI: 10.1109/IEEESTD.2018.8299595
---
